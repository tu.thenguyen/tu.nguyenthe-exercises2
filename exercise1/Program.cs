﻿using System;

namespace exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 45; i++)
            {
                Console.Write(Fibonacci(i)+" ");
            }
        }
        static int Fibonacci(int check)
        {
            int first_F = 0;
            int second_F = 1;
            int result_F = 1;

            if (check == 0 || check == 1)
            {
                return check;
            }
            else
            {
                for (int i = 2; i < check; i++)
                {
                    first_F = second_F;
                    second_F = result_F;
                    result_F = first_F + second_F;
                }
                return result_F;
            }

        }
    }
}
